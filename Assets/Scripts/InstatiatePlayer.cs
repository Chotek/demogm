﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstatiatePlayer : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        if(player != null)
        {
            Transform.Instantiate(player);
        }        
    }

}
