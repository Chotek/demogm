﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gère le comportement de la caméra TPS
/// </summary>
public class CameraFollowBehaviour : MonoBehaviour
{
    /// <summary>
    /// constante contenant la valeur minimum de rotation sur l'axe X 
    /// </summary>
    private const float CAM_MIN_Y = 5.0f;
    /// <summary>
    /// constante contenant la valeur maximum de rotation sur l'axe X 
    /// </summary>
    private const float CAM_MAX_Y = 60.0f;

    /// <summary>
    /// cible de la caméra
    /// </summary>
    GameObject target;

    /// <summary>
    /// contient la valeur du mouvement de la souris sur l'axe X
    /// </summary>
    private float currentX = 0.0f;
    /// <summary>
    /// contient la valeur du mouvement de la souris sur l'axe Y
    /// </summary>
    private float currentY = 0.0f;

    /// <summary>
    /// distance de la caméra par rapport à la cible
    /// </summary>
    public float distance = 10.0f;

    private void Update()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY += Input.GetAxis("Mouse Y");

        //restraint la rotation de la caméra sur l'axe X entre CAM_MIN_Y et CAM_MAX_Y
        currentY = Mathf.Clamp(currentY, CAM_MIN_Y, CAM_MAX_Y);
    }

    void LateUpdate()
    {
        // si aucune cible n'existe, choisi le joueur
        if(target == null)
        {
            target = GameObject.FindWithTag("Player");
        }
        
        Vector3 camPosition = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);

        transform.position = target.transform.position + rotation * camPosition;

        transform.LookAt(target.transform.position);
        
    }
}
