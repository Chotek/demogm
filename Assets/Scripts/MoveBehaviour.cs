﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBehaviour : MonoBehaviour
{
    public int moveSpeed = 1;
    public int rotationSpeed = 50;



    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Z))
        {
            this.gameObject.transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
        else if(Input.GetKey(KeyCode.S))
        {
            this.gameObject.transform.Translate(Vector3.forward * -moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            this.gameObject.transform.Rotate(Vector3.up * -rotationSpeed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.gameObject.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }
    }
}
