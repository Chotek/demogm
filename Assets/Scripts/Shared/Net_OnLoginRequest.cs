﻿[System.Serializable]
public class Net_OnLoginRequest : NetMsg
{
    public Net_OnLoginRequest()
    {
        OP = NetOP.OnLoginRequest;
    }

    /// <summary>
    /// Value to define , exemple : 0 -> success, 1 -> missing username, etc
    /// </summary>
    public byte success;

    public int connection_id;
    public string username;

    /// <summary>
    /// a discriminator is something like : username#0001 -> 0001 is the discriminator
    /// </summary>
    public string discriminator;
    public string token;

}
