﻿[System.Serializable]
public class Net_OnCreateAccount : NetMsg
{
    public Net_OnCreateAccount()
    {
        OP = NetOP.OnCreateAccount;
    }

    /// <summary>
    /// Value to define , exemple : 0 -> success, 1 -> missing username, etc
    /// </summary>
    public byte success;
}
