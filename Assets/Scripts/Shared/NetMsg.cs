﻿public static class NetOP
{
    public const int None = 0;

    public const int CreateAccount = 1;
    public const int LoginRequest = 2;

    #region Callbacks

    public const int OnCreateAccount = 101;
    public const int OnLoginRequest = 102;

    #endregion

}
[System.Serializable]
public abstract class NetMsg 
{
    //operation code
    public byte OP { set; get; }

    public NetMsg()
    {
        OP = NetOP.None;
    }

}
