﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Temporary class to load the game scene
/// </summary>
public class LoadScene : MonoBehaviour
{
    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

}
