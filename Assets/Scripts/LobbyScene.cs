﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyScene : MonoSingleton<LobbyScene>
{
    /// <summary>
    /// send the creation account information to the server
    /// </summary>
    public void OnClickCreateAccount()
    {
        DisableInput();

        string username = GameObject.Find("CreateUsername").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("CreatePassword").GetComponent<TMP_InputField>().text;
        string email = GameObject.Find("CreateEmail").GetComponent<TMP_InputField>().text;

        Client.Instance.SendCreateAccount(username, password, email);
    }

    /// <summary>
    /// Send the login request informations to the server
    /// </summary>
    public void OnClickLogin()
    {
        DisableInput();

        string username = GameObject.Find("Login").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("Password").GetComponent<TMP_InputField>().text;

        Client.Instance.SendLogin(username, password);
    }

    /// <summary>
    /// enable inputs of the whole canvas
    /// </summary>
    public void EnableInput()
    {
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    /// <summary>
    /// Disable inputs of the whole canvas
    /// </summary>
    public void DisableInput()
    {
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;
    }

    public void ChangeAuthentificationMessage(string msg)
    {
        GameObject.Find("AuthentificationMessageText").GetComponent<TextMeshProUGUI>().text = msg;
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("Game");
    }
}
