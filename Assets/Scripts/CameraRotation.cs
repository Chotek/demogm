﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public float rotationSpeed = 50f;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.LookAt(target);
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");
        Vector3 rotateValue = new Vector3(y, -x, 0) * rotationSpeed * Time.deltaTime;

        transform.eulerAngles = transform.eulerAngles - rotateValue;
        
    }
}
