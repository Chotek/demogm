﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// processed anything to do with sending and receiving datas from and towards the server
/// TODO REF1 : create a singleton abstract class (which inherit for monobehaviour) and make Client inherit this new class, so that only one instance of this object can be present  
/// </summary>
public class Client : MonoSingleton<Client>
{
    #region Const
    /// <summary>
    /// Number of player that can connect to the server
    /// </summary>
    private const int MAX_USER = 100;
    /// <summary>
    /// port on which informations pass 
    /// </summary>
    private const int PORT = 60000;
    /// <summary>
    /// Ip of the server
    /// </summary>
    private const string SERVER_IP = "127.0.0.1";
    private const int BYTE_SIZE = 1024;
    #endregion

    #region Variables
    /// <summary>
    /// reliable channel : we are sure to get the information but not the order in which it is received
    /// </summary>
    private byte reliableChannel;
    /// <summary>
    /// id of the client
    /// </summary>
    private int hostId;
    /// <summary>
    /// the id of the client connected.
    /// </summary>
    private int connectionId;
    /// <summary>
    /// error of the connection (displayed as number), the documentation can be found on https://docs.unity3d.com 
    /// </summary>
    private byte error;
    private bool isConnected;
    #endregion

    #region Monobehaviour
    private void Start()
    {
        // to not lose the connection between scenes changes.
        DontDestroyOnLoad(gameObject);

        Init();
    }

    private void Update()
    {
        UpdateMessagePump();
    }
    #endregion

    /// <summary>
    /// to initialyse the connection towards the server.
    /// </summary>
    public void Init()
    {
        NetworkTransport.Init();

        ConnectionConfig cc = new ConnectionConfig();
        reliableChannel =  cc.AddChannel(QosType.Reliable);

        // must be identical to the server topology
        HostTopology topo = new HostTopology(cc, MAX_USER);

        //Client only code
        hostId = NetworkTransport.AddHost(topo,0);

        connectionId = NetworkTransport.Connect(hostId, SERVER_IP, PORT, 0, out error);
        Debug.Log(string.Format("Attemping to connect on {0} ...", SERVER_IP));
        isConnected = true;

    }

    public void Shutdown()
    {
        isConnected = false;
        NetworkTransport.Shutdown();
    }

    /// <summary>
    /// reception of event on the network
    /// </summary>
    public void UpdateMessagePump()
    {
        if (!isConnected)
        {
            return;
        }

        int recHostId; // id of the plateform - alway 1 for client 
        int connectionId; // which user is sending the message - always 1 for client
        int channelId; // Which lane is used to send the message (reliable, etc);

        byte[] recBuffer = new byte[BYTE_SIZE];
        int dataSize;

        NetworkEventType type = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, BYTE_SIZE, out dataSize, out error);

        switch (type)
        {
            case NetworkEventType.Nothing:
                break;
            case NetworkEventType.ConnectEvent:
                Debug.Log(string.Format("We have connected to the server ! (id : {0})", connectionId));
                break;
            case NetworkEventType.DisconnectEvent:
                Debug.Log("We have been deconnected.");
                break;
            case NetworkEventType.DataEvent:
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream ms = new MemoryStream(recBuffer);
                NetMsg msg = (NetMsg)formatter.Deserialize(ms);

                OnData(connectionId, channelId, recHostId, msg);
                break;

            default:
            case NetworkEventType.BroadcastEvent:
                Debug.Log("Unexpected network event type.");
                break;

        }


    }

    #region OnData
    private void OnData(int connId, int channelId, int recHostId, NetMsg msg)
    {
        switch (msg.OP)
        {
            case NetOP.None:
                Debug.Log("Unexpected NET_OP");
                break;
            case NetOP.OnCreateAccount:
                OnCreateAccount((Net_OnCreateAccount)msg);
                break;
            case NetOP.OnLoginRequest:
                OnLoginRequest((Net_OnLoginRequest)msg);
                break;
        }
    }

    /// <summary>
    /// On reception of the Create account request message form the server
    /// </summary>
    /// <param name="oca">the server response</param>
    private void OnCreateAccount(Net_OnCreateAccount oca)
    {
        LobbyScene.Instance.EnableInput();
        if(oca.success == 0)
        {
            LobbyScene.Instance.ChangeAuthentificationMessage("Your account has been created !");
        }
        else if(oca.success == 2 )
        {
            LobbyScene.Instance.ChangeAuthentificationMessage("the username or email already exist !");
        }
        else
        {
            // unexpected failure.
            LobbyScene.Instance.ChangeAuthentificationMessage("the creation of your account has failed");
        }        
    }

    /// <summary>
    /// On reception of the login request message form the server
    /// </summary>
    /// <param name="olr">the server response</param>
    private void OnLoginRequest(Net_OnLoginRequest olr)
    {
        
        if (olr.success == 0)
        {
            Debug.Log(string.Format("Welcome {0} !", olr.username));
            LobbyScene.Instance.ChangeAuthentificationMessage(string.Format("Welcome {0}", olr.username));
            LobbyScene.Instance.LoadGameScene();
        }
        else
        {
            LobbyScene.Instance.ChangeAuthentificationMessage("login failed !");
            LobbyScene.Instance.EnableInput();
        }
    }
    #endregion

    #region Send
    /// <summary>
    /// Use to send information to the server
    /// </summary>
    /// <param name="msg">The informations you are sending</param>
    public void SendServer(NetMsg msg)
    {
        // this were we hold out data
        byte[] buffer = new byte[BYTE_SIZE];

        // this is where you would crush your data into a byte[]
        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream ms = new MemoryStream(buffer);
        formatter.Serialize(ms, msg);


        NetworkTransport.Send(hostId, connectionId, reliableChannel, buffer, BYTE_SIZE, out error);
        
        // if the client cannot acces the server and we are on the Lobby scene 
        if(error != 0 && LobbyScene.Instance != null)
        {
            LobbyScene.Instance.ChangeAuthentificationMessage("Connection to the server failed!");
            LobbyScene.Instance.EnableInput();
        }
    }

    /// <summary>
    /// Send a message to the server to create an account in the database
    /// </summary>
    /// <param name="username">username of the user</param>
    /// <param name="password">password of the user</param>
    /// <param name="email">email of the user</param>
    public void SendCreateAccount(string username,string password, string email)
    {
        Net_CreateAccount ca = new Net_CreateAccount();

        ca.username = username;
        ca.password = password; // TO DO : hash the password before transfert
        ca.email = email;

        SendServer(ca);
    }

    /// <summary>
    /// Send a message to the server to login
    /// </summary>
    /// <param name="usernameOrEmail"> username or email of the user</param>
    /// <param name="password">password of the user</param>
    public void SendLogin(string usernameOrEmail, string password)
    {
        Net_LoginRequest ca = new Net_LoginRequest();

        ca.usernameOrEmail = usernameOrEmail;
        ca.password = password; // TO DO : hash the password before transfert

        SendServer(ca);
    }

    #endregion
    
}
